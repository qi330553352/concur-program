package com.example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 创 建 时 间: 2019/8/29
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@RestController
@Api(value = "专利名称组件")
@Resource(name = "对外接口")
//@RequestMapping("web")
public class WebController {


    @GetMapping("test/{switched}")
    @ApiImplicitParam(name = "switched",required = true,defaultValue = "on")
    @ApiOperation(value = "根据ID获得对象", notes = "对象查询", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public long test(@PathVariable(value = "switched") String switched){
        log.info("--------test---------switched:{}",switched);

        return System.currentTimeMillis();
    }
}
