package com.example.threads;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 创 建 时 间: 2019/8/31
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class SpringThreadPool {

    private static ThreadPoolTaskExecutor pool;

    private SpringThreadPool() {

    }

    public static ThreadPoolTaskExecutor getInstance(){
        if(pool==null){
            pool = getThreadPool();
        }
        return pool;
    }

    private static ThreadPoolTaskExecutor getThreadPool() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        //最小线程数
        pool.setCorePoolSize(2);
        //最大线程数
        pool.setMaxPoolSize(4);
        //空闲线程存活时间
        pool.setKeepAliveSeconds(3);
        //队列中最大线程
        pool.setQueueCapacity(4);

        pool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //threadPoolTaskExecutor.setThreadFactory(new MultiThreadFactory());
        // 等待所有任务结束后再关闭线程池
        //pool.setWaitForTasksToCompleteOnShutdown(true);
        //初始化
        pool.initialize();
        return pool;
    }

}
