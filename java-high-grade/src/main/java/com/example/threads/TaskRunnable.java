package com.example.threads;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * 创 建 时 间: 2019/9/5
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class TaskRunnable implements Callable<Integer> {

    private Integer idx;
    private CountDownLatch countDownLatch;

    public TaskRunnable(Integer idx, CountDownLatch countDownLatch) {
        this.idx = idx;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public Integer call() throws Exception {
        int result = 1;
        System.out.println(Thread.currentThread().getName());
        try {
            int a = 0;
            if (idx == 1) {
               //a = 1 / 0;
                Thread.sleep(1000L);
            }
        }catch (Exception e){
            result = 0;
            e.printStackTrace();
        } finally {
            countDownLatch.countDown();
        }
        return result;
    }
}
