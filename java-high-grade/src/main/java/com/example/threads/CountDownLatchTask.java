package com.example.threads;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;

/**
 * 创 建 时 间: 2019/9/5
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class CountDownLatchTask {

    /**
     * 只有 countDown() 数量等于 执行的任务数量 await() 才会继续执行
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ThreadPoolTaskExecutor pool = SpringThreadPool.getInstance();
        CountDownLatch countDownLatch = new CountDownLatch(3);
        int result = 0;
        List<Future<Integer>> tasks = new ArrayList<>();
        for(int i=0;i<3;i++){
            tasks.add(pool.submit(new TaskRunnable(i,countDownLatch)));
        }
//        Future<Integer> task1 = pool.submit(new TaskRunnable(1,countDownLatch));
//        Future<Integer> task2 = pool.submit(new TaskRunnable(2,countDownLatch));
//        Future<Integer> task3 = pool.submit(new TaskRunnable(3,countDownLatch));
        countDownLatch.await();
//        result = (task1.get()+task2.get()+task3.get());
        for(Future<Integer> future : tasks)
            result += future.get();
        System.out.println("qixin:"+Thread.currentThread().getName()+" result:"+result);
        pool.shutdown();
    }

}
