package com.example.threads;

import java.util.concurrent.ThreadFactory;

/**
 * 创 建 时 间: 2019/8/31
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class MultiThreadFactory implements ThreadFactory {

    private String PREFIX_NAME = "multi-thread-";
    private int i = 0;
    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setName(PREFIX_NAME+this.i++);
        return thread;
    }
}
