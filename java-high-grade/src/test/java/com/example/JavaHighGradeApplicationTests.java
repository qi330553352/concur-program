package com.example;

import com.example.threads.SpringThreadPool;
import com.example.threads.ThreadPool;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ThreadPoolExecutor;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaHighGradeApplicationTests {

	@Test
	public void contextLoads() {

		ThreadPoolTaskExecutor threadPool = SpringThreadPool.getInstance();
		for (int i=0;i<20;i++){
			threadPool.execute(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(200L);
						log.info(Thread.currentThread().getName());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
		try {
			Thread.sleep(1000 * 5);
		}catch (Exception e){
			e.printStackTrace();
		}
		log.info("========主线程结束==========");
	}

}
